package br.com.itau.ClienteCaseAcesso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ClienteCaseAcessoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClienteCaseAcessoApplication.class, args);
	}

}
