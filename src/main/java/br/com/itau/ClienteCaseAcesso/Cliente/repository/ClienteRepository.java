package br.com.itau.ClienteCaseAcesso.Cliente.repository;

import br.com.itau.ClienteCaseAcesso.Cliente.model.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository <Cliente, Integer> {
}
