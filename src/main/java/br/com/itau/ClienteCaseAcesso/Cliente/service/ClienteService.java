package br.com.itau.ClienteCaseAcesso.Cliente.service;

import br.com.itau.ClienteCaseAcesso.Cliente.model.Cliente;
import br.com.itau.ClienteCaseAcesso.Cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criarCliente (Cliente cliente){
        Cliente clienteObjeto = clienteRepository.save(cliente);
        return clienteObjeto;
    }

    public Optional<Cliente> buscarPorId (int id){
        Optional<Cliente> cliente = clienteRepository.findById(id);
        if (cliente.isPresent()){
            return cliente;
        }
        throw new RuntimeException("O cliente não foi encontrado");
    }
}
