package br.com.itau.ClienteCaseAcesso.Cliente.controller;

import br.com.itau.ClienteCaseAcesso.Cliente.model.Cliente;
import br.com.itau.ClienteCaseAcesso.Cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente criarCliente (@RequestBody @Valid Cliente cliente){
        Cliente clienteObjeto = clienteService.criarCliente(cliente);
        return clienteObjeto;
    }

    @GetMapping("/{id}")
    public Optional<Cliente> buscarPorId (@PathVariable(name = "id") int id){
        try{
            Optional<Cliente> cliente = clienteService.buscarPorId(id);
            return cliente;
        }catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }
}
